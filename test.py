#app utile anche in strutture complesse (ministeri, università, ecc)

import json
import math
from myClassifier import MyClassifier
import sys

#leggiamo i documenti dal json; creiamo quindi la nostra lista di descrizioni dei monumenti e dei nomi degli stessi
monumentFile = open('./res/monumentiseri.json', encoding='utf-8')
json_data = monumentFile.read()
data = json.loads(json_data)
monumentFile.close()

monuments = data['monumenti']

classifier = MyClassifier('./res/domande.csv')
ids = [];
current_id =-2

while(current_id != -1):
    print("Dove ti trovi?")
    for monument in monuments:
        print(monument["ID"], ": " ,monument["Nomi"][0])
        ids.append(monument["ID"])
        for children in monument["Children"]: 
            print(children["ID"], ": " ,children["Nomi"][0])
            ids.append(children["ID"])
    
    current_id = -1
    while (current_id not in ids):
        current_id = input("Inserisci uno tra gli ID presentati, inserisci -1 per uscire: ")
        current_id = int(current_id)
        if(current_id == -1):
            sys.exit()
    
    question = input("Fammi una domanda su questo argomento! ")
    tag = classifier.classifyQuestion(question)
    
    if current_id < 100:
        try:
            answer = monuments[current_id][tag[0]]
        except KeyError:
            answer = "Non ho una risposta, mi spiace!\n"
    else:
        try:
            answer = monuments[math.floor(current_id/100)-1]["Children"][current_id-100][tag[0]]
        except KeyError:
            try:
                answer = monuments[math.floor(current_id/100)-1][tag[0]]
            except KeyError:
                answer = "Non ho una risposta, mi spiace!\n"
    print(answer)
