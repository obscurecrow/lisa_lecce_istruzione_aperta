# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 21:53:46 2018

@author: Byakuya
"""

import pandas
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.svm import LinearSVC

df = pandas.read_csv('./res/domande.csv')
df.columns = ['QUESTION', 'TAG']

X_train, X_test, y_train, y_test = train_test_split(df['QUESTION'], df['TAG'], random_state = 0)
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)

classifier = LinearSVC().fit(X_train_tfidf, y_train)

question = "Chi è l'artista della statua della libertà?"
print(classifier.predict(count_vect.transform([question])))
