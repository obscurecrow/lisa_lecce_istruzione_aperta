# -*- coding: utf-8 -*-
"""
Created on Thu May 10 19:10:59 2018

@author: Byakuya
"""
import pandas
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.svm import LinearSVC


class MyClassifier:
    
    classifier = []
    count_vect = []
    
    def __init__(self, csvFile):        
        df = pandas.read_csv(csvFile)
        df.columns = ['QUESTION', 'TAG']
                
        X_train, X_test, y_train, y_test = train_test_split(df['QUESTION'], df['TAG'], random_state = 0)
        self.count_vect = CountVectorizer()
        X_train_counts = self.count_vect.fit_transform(X_train)
        tfidf_transformer = TfidfTransformer()
        X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
        
        self.classifier = LinearSVC().fit(X_train_tfidf, y_train)

        
    def classifyQuestion(self,question):
        return self.classifier.predict(self.count_vect.transform([question]))