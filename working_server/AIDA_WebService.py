import cherrypy
from AIDA_WebServer import AIDAwebserver

@cherrypy.expose
class AIDA_WebService(object):
    aida = []
    def __init__(self):
        self.aida = AIDAwebserver()    
    
    @cherrypy.tools.accept(media='text/plain')

    def POST(self, myID, question):
        answer = self.aida.findAnswer(myID, question)
        return answer


if __name__ == '__main__':
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')]
        }
    }
    cherrypy.config.update({'server.socket_port': 9800,})
    cherrypy.quickstart(AIDA_WebService(), '/', conf)